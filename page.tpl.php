<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $language->language ?>" xml:lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>" id="html-main">
<head>
  <title><?php echo $head_title ?></title>
  <?php echo $head ?>
  <?php echo $styles ?>
  <?php echo $scripts ?>
  <script type="text/javascript"></script>
</head>

<body>
  <!--Start primarymenu-->
  <?php if (isset($primary_links)) : ?>
  <div class="topmenu">
  <!-- admin edit   -->
  <?php if ($is_admin): ?><?php echo l(t("Edit menu"), "admin/build/menu-customize/primary-links", array("attributes" => array("class" => "edit-this-link"))); ?><?php endif; ?>
  <!-- admin edit   -->
    <div class="topmenu_right"></div>
    <?php echo theme('links', $primary_links) ?>
  <?php endif; ?>
  </div>
  <!--End primarymenu-->

  <div class="header">
    <?php if ($logo) { ?>
    <div id="logo-picture">
      <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
    </div><?php } ?>

    <?php if ($site_name) { ?><h3 id='sitetitle'><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h3><?php } ?>
    <?php if ($site_slogan) { ?><h3 id='subtitle'><?php print $site_slogan ?></h3><?php } ?>
  </div>

  <!--Start Page-->
  <div class="page">
    <?php if ($search_box): ?>
      <div id="search-box">
      <?php print $search_box; ?>
      </div><!-- /search-box -->
      <span class="small">&nbsp;</span>
    <?php endif; ?>
    <hr />

    <!--Start col_left-->
    <div class="col_left">
      <!-- admin panel   -->
      <?php if ($is_admin): ?>
        <ul id="rws-uni-tabs" class="clear-block">
          <li><?php echo l(t("Administer"), "admin"); ?></li>
          <li><?php echo l(t("Blocks"), "admin/build/block"); ?></li>
          <li><?php echo l(t("Menus"), "admin/build/menu"); ?></li>
          <li><?php echo l(t("Modules"), "admin/build/modules"); ?></li>
          <li><?php echo l(t("Translation"), "admin/build/translate/search"); ?></li>
        </ul>
      <?php endif; ?>
      <!-- / admin panel -->
      <?php echo $left ?>
    </div>
    <!--End col_left-->

    <!--Start col_right-->
    <?php if ($right): ?>
    <div class="col_right">
      <?php echo $right ?>
    </div>
    <?php endif; ?>
    <!--End col_right-->

    <!--Start col_center-->
    <div class="col_center<?php if (empty($right) AND !empty($left)) {echo '_755';} if (empty($right) AND empty($left)) {echo '_935';} if (!empty($right) AND empty($left)) {echo '_632';} ?>">
      <?php if ($show_messages): ?>
        <?php echo $messages; ?>
      <?php endif; ?>
      <?php if ($title): ?><h1 class="title"><?php echo $title ?></h1><?php endif; ?>
      <?php if ($tabs): ?><div class="tabs"><?php echo $tabs; ?></div><?php endif; ?>
      <!-- main-content-block -->
      <div class="main-content-block">
      <?php echo $content; ?>
      </div>
      <!-- /main-content-block -->
      <!-- content after blocks -->
      <?php if ($content_after_blocks): ?>
      <div class="content_after_blocks">
      <?php echo $content_after_blocks ?>
      </div>
      <?php endif; ?>
      <!-- after content block end-->
    </div>
    <!--End col_center-->
  <div class="clear-both"></div>
  </div>
  <!--End Page-->

  <!--Start footer-->
  <div class="footer_top"></div>
  <div class="footer">
  <?php if ($footer_block_left_1 || $footer_block_left_2 || $footer_block_right_1 || $footer_block_right_2): ?>
    <!-- /footer-blocks -->
    <div class="clear-both"></div>
    <div class="footer-blocks">
    <?php if ($footer_block_right_2): ?><?php echo $footer_block_right_2 ?><?php endif; ?>
    <?php if ($footer_block_right_1): ?><?php echo $footer_block_right_1 ?><?php endif; ?>
    <?php if ($footer_block_left_2): ?><?php echo $footer_block_left_2 ?><?php endif; ?>
    <?php if ($footer_block_left_1): ?><?php echo $footer_block_left_1 ?><?php endif; ?>
    </div>
    <!-- /footer-blocks -->
  <?php endif; ?>
  <div class="clear-both">
  <?php echo $footer ?>
  <?php echo $footer_message ?>
  </div>
  <div class="clear-both">&nbsp;</div>
  <!--End footer-->
</div>
<?php echo $closure ?>
</body>
</html>